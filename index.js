const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
app.use(bodyParser.json());
app.use(cors)

app.listen(process.env.PORT || 3000, () => {
    console.log('connected')
});

app.get('/', (req, res) => {
    res.status(200).json({
        success: true
    })
})